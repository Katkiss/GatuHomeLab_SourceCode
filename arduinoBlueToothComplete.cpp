#include <SoftwareSerial.h>   //Software Serial Port
#include <string.h>
#define RxD 2
#define TxD 3
SoftwareSerial blueToothSerial(RxD,TxD);

//Sensor Data
char ObjectID[]="2"; //1 - for vibration sensor // 5 - for pressure 2 wire (SCL pin)
int AnalogIn=5;  //SCL pin is Analog Input 5

boolean conn=false;

char btstr[80];//100
char btinfo;

char trama[50]; //100
char info[40]; //80
char aux[20];

char tramainit[]={'S','T','R','\0'};
char tramaend[]={'E','T','R','\0'};
char tramacomm01[]={'I','D','\0'};
int btstrlen;

//char auxdur[10];

void setup()
{
    Serial.begin(9600);   
    //BlueTooth
    pinMode(RxD, INPUT);
    pinMode(TxD, OUTPUT);
    //Serial.println("Starting BT...");
    setupBlueToothConnection();    
}

void setupBlueToothConnection()
{
    //Serial.println(F(".."));
    blueToothSerial.begin(38400); //Set BluetoothBee BaudRate to default baud rate 38400
    delay(1000);
    sendBlueToothCommand("\r\n+STWMOD=0\r\n");  //device working as slave (0) master (1)
    sendBlueToothCommand("\r\n+STNA=BTB02-st\r\n"); //device name
    sendBlueToothCommand("\r\n+STAUTO=0\r\n"); // 0 // permit auto connect (1)
    sendBlueToothCommand("\r\n+STOAUT=1\r\n"); // not permit paired device to connect me (0)
    sendBlueToothCommand("\r\n+STPIN=0000\r\n"); // pin code
    //sendBlueToothCommand("\r\n+LOSSRECONN=0\r\n"); // permit auto-reconnecting
    delay(2000); // This delay is required.
    sendBlueToothCommand("\r\n+INQ=1\r\n"); // enable been inquired (1) when slave by master
    delay(2000); // This delay is required.
    //blueToothSerial.listen();
    //sendBlueToothCommand("\r\n+CONN=43,29,B1,55,01,01\r\n");
    Serial.println(F("setupBTconn: listening..."));
    while(!conn){
      if(blueToothSerial.available()>0){
        CheckOK();
        conn=true;
        Serial.println(F("SetupBTconn: ok"));
      }
    }
}

boolean foodbowl=false;
boolean tramafound=false;
int sensingvalue=100;

void loop() // run over and over
{ 

      char aux[15];
      //read analog signal
      int value=analogRead(AnalogIn);
      Serial.print("Analog Value: ");
      Serial.println(value);
      if(value>sensingvalue) foodbowl=true;
  
  
      //read available
      while(blueToothSerial.available()>0){     
        btinfo=blueToothSerial.read();
        strcat(btstr,&btinfo);
        //btstr.concat(btinfo);     
        //Serial.print(F("Receive BT trama: "));
        //Serial.println(btstr);  
        delay(100);
      }
       
      
      //STRIDETR 
      //01234567
      btstrlen=strlen(btstr);
      
      
      if(btstrlen>5){
        Serial.print("--debug trama length: ");
        Serial.println(btstrlen);
        int auxindex=0;
        char *s;  
        s = strstr(btstr, "STR");
        if (s != NULL){
          Serial.print("Found Trama at index ");
          auxindex=s-btstr;
          Serial.println(auxindex);
          Serial.println(&btstr[auxindex]);
          tramafound=true;
          //getting data
          char *p=&btstr[auxindex];
          char *str;
          str=strtok_r(p,"#",&p);
          str=strtok_r(NULL,"#",&p);
          sensingvalue=atoi(str);
          Serial.print("Sensing Value: ");
          Serial.println(sensingvalue);
        }else{
           Serial.println("Trama not found");  // `strstr` returns NULL if search string not found
        }
        
        if(tramafound){ //command - ID - send the sensor ID
            strcpy(info,"FD");
            if(foodbowl==true){
              ObjectID[0]='1';
              strcat(info,ObjectID);
            }else{
              //ObjectID[0]='0';
              sprintf(aux,"%d",value);
              strcat(info,aux);
            }
            //strcat(info,ObjectID);
            
            Serial.println(F("Sending Result... ")); 
            BTsend();
            foodbowl=false;
            tramafound=false;
        }
      }
        
       
      //Serial.println("Poniendo a cero btstr");  
      strcpy(btstr,"\0");
      delay(300); //300
       
}

void BTsend(){
    //Serial.print(F("Sending info: "));
    strcpy(trama,"STR");
    strcat(trama,info);
    strcat(trama,"ETR");
    blueToothSerial.println(trama);
    strcpy(trama,"\0");
}


//Checks if the response "OK" is received
void CheckOK()
{
  char a,b;
  while(1)
  {
    if(blueToothSerial.available())
    {
    a = blueToothSerial.read();
    if('O' == a)
    {
      // Wait for next character K. available() is required in some cases, as K is not immediately available.
      while(blueToothSerial.available()) 
      {
         b = blueToothSerial.read();
         break;
      }
      if('K' == b)
      {
        break;
      }
    }
   }
  }
 
  while( (a = blueToothSerial.read()) != -1)
  {
    //Wait until all other response chars are received
  }
}
 
void sendBlueToothCommandOld(char command[])
{
    blueToothSerial.print(command); 
    //---------------
    while(blueToothSerial.available()>0){     
        btinfo=blueToothSerial.read();
        strcat(btstr,&btinfo);
        //btstr.concat(btinfo);     
        Serial.print(F("Receive BT trama: "));
        Serial.println(btstr);  
        //delay(100);
    }
    if(btstrlen>25){
      return;
    }
    //---------------
    CheckOK();   
}

void sendBlueToothCommand(char command[])
{
    char a;
    blueToothSerial.print(command);
    //Serial.print(command);                          //For debugging, Comment this line if not required    
    delay(3000);
}