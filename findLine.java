import georegression.struct.line.LineParametric2D_F32;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.util.List;

import boofcv.abst.feature.detect.line.DetectLineHoughFoot;
import boofcv.abst.feature.detect.line.DetectLineHoughFootSubimage;
import boofcv.abst.feature.detect.line.DetectLineHoughPolar;
import boofcv.core.image.ConvertBufferedImage;
import boofcv.factory.feature.detect.line.ConfigHoughFoot;
import boofcv.factory.feature.detect.line.ConfigHoughFootSubimage;
import boofcv.factory.feature.detect.line.ConfigHoughPolar;
import boofcv.factory.feature.detect.line.FactoryDetectLineAlgs;
import boofcv.gui.feature.ImageLinePanel;
import boofcv.gui.image.ShowImages;
import boofcv.struct.image.ImageSingleBand;

public class findLine {
	
		// adjusts edge threshold for identifying pixels belonging to a line
		private static final float edgeThreshold = 10;
		// adjust the maximum number of found lines in the image
		private static final int maxLines = 1;
		
		//result angle from line detection
		public double LineAngle=0.0;
		
		//constructor
		public findLine(){
			//do nothing...
		}
		
		@SuppressWarnings("rawtypes")
		public <T extends ImageSingleBand, D extends ImageSingleBand> boolean detectLines1( BufferedImage image , Class<T> imageType ,Class<D> derivType ){
			
			// convert the line into a single band image
			T input = ConvertBufferedImage.convertFromSingle(image, null, imageType );
		
			// line detector
			DetectLineHoughPolar<T,D> detector = FactoryDetectLineAlgs.houghPolar(
					new ConfigHoughPolar(3, 30, 2, Math.PI / 1800,edgeThreshold, maxLines), imageType, derivType);
			
			List<LineParametric2D_F32> found = detector.detect(input);
			
			if(found.size()<1) return false;
		
			LineAngle=(double)found.get(0).getAngle();
			LineAngle=Math.toDegrees(LineAngle);
			System.out.println("FINDLINE:: Angle "+LineAngle);
			
			/*
			// display the results
			ImageLinePanel gui = new ImageLinePanel();
			gui.setBackground(image);
			gui.setLines(found);
			gui.setPreferredSize(new Dimension(image.getWidth(),image.getHeight()));
			double angle=(double)found.get(0).getAngle();
		
			ShowImages.showWindow(gui,"Found Lines 1 "+Math.toDegrees(angle));
			*/
			
			return true;
		}
}