package com.sensorlab.comm;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class LinuxBTCommunicationTrama {
	//variables
	private InputStream is;
	private OutputStream os;
	byte[] start;
	byte[] end;
	byte[] tramareceived;
	String sttramareceived;
	byte[] tramasent;
	int trindex=0;

	
	
	//constructor
	public LinuxBTCommunicationTrama(InputStream is, OutputStream os, byte[] start, byte[] end){
		this.is=is;
		this.os=os;
		this.start=start;
		this.end=end;
	}
	
	//get/identify and return trama from inputstream
	public byte[] getTrama(){		
		//System.out.println("BTHttpTask - BTCOMM - getTrama");
		sttramareceived=new String();
		tramareceived=new byte[1025];
		trindex=0;
		boolean findtrama=false;
		while(!findtrama){
			if(!getBTbytes()) return null;
			//analize data
			String staux=new String(start);
			String edaux=new String(end);
			if(((sttramareceived.indexOf(staux)+1)*(sttramareceived.indexOf(edaux)))>1){ //WE ADD +1 BECAUSE INDEX IS 0 IS OK
				findtrama=true;
			}
			//System.out.println("BTHttpTask - BTCOMM - getTrama "+(sttramareceived.indexOf(staux)));
		} 
		return tramareceived;
	}
	
	//get bytes from BT input stream
	private boolean getBTbytes(){		
		try{
			while(is.available()>0){					
				byte[] btinfo = new byte[is.available()];
				is.read(btinfo);
				for(int i=0;i<btinfo.length;i++){
					sttramareceived+=(char)btinfo[i];
					tramareceived[trindex+i]=btinfo[i];
				}
				trindex+=btinfo.length;
				//System.out.println("BTHttpTask - BTCOMM - trindex-length "+trindex);
				//System.out.println("BTHttpTask - BTCOMM - getTrama "+sttramareceived.toString());
			}						
		} catch (IOException e) {
				System.out.println("-");
				return false;
		}
		return true;
	}
	
	//send trama to outputstream - command/info
	public boolean sendTrama(byte[] command, byte[] info){
		//lets construct the trama
		int Tlength=start.length+command.length+info.length+end.length;
		tramasent=new byte[Tlength];
		System.arraycopy(start, 0, tramasent, 0, start.length);
		System.arraycopy(command,0,tramasent, start.length, command.length);
		System.arraycopy(info, 0, tramasent, (start.length+command.length), info.length);
		System.arraycopy(end, 0, tramasent, (Tlength-end.length), end.length);
		//send trama
		try {
			os.write(tramasent);
			os.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		//System.out.println("BTHttpTask - BTCOMM - sendTrama ok: "+tramasent.toString());
		return true;
	}
	
	//send trama only with command
	// STR AA ETR
	public boolean sendTrama(byte[] command){
		tramasent=new byte[start.length+end.length+command.length];
		System.arraycopy(start, 0, tramasent, 0, start.length);
		System.arraycopy(command, 0, tramasent, start.length, command.length);
		System.arraycopy(end,0,tramasent,(start.length+command.length),end.length);
		//send trama
		try {
			os.write(tramasent);
			os.flush();		
		}catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	//get command from current trama available
	//command are 2 bytes after start array
	public String getTramaCommand(){
		return sttramareceived.substring(start.length, start.length+2);
	}
	
	
	//get info from current trama available   
	// STRID3ETR
	// 012345678
	public byte[] getTramaInfo(){ 
		byte[] info=new byte[trindex-start.length-end.length-2-2]; //we have to add -2 because of end of line characters       
		//System.out.println("BTHttpTask - BTCOMM - tramaInfo "+trindex);
		//System.out.println("BTHttpTask - BTCOMM - tramaInfo "+sttramareceived);
		//System.out.println("BTHttpTask - BTCOMM - infoLength "+info.length);
		System.arraycopy(tramareceived, (start.length+2), info, 0, info.length);
		return info;
	}
	
	public String getTramaInfoSt(){
		return new String(getTramaInfo());
	}
	
	//get data received in the form
	// STR#12#345#ABCD#789#ETR
	public String[] getTramaMultipleInfoSt(){
		String tramainfo=getTramaInfoSt();
		String[] data=tramainfo.split("#");
		//System.out.println("BTHttpTask - BTCOMM - MultipleInfo "+data.length);
		return data;
	}
	
	

}