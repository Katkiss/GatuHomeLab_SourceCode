import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import boofcv.io.image.UtilImageIO;
import boofcv.struct.image.ImageSInt16;
import boofcv.struct.image.ImageUInt8;
import com.sensorlab.image.findLine;


public class CalibrateServoUsingCamera {
	
	public static void main( String args[] ) {
		
		//write results to file
		File file = new File("/Users/borja/Desktop/BeagleBone/images/ExampleResultTiltCalibration.csv");
		FileWriter fw = null;
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			fw = new FileWriter(file.getAbsoluteFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedWriter bw = new BufferedWriter(fw);
		double auxAngle=0.0;
		
		for(double dci=500.0;dci<2300.0;dci+=1.0){
			BufferedImage input = UtilImageIO.loadImage("/Users/borja/Desktop/BeagleBone/images/"+dci+".jpg");
			findLine fl=new findLine(); 
			fl.detectLines1(input,ImageUInt8.class,ImageSInt16.class);
			
			try {
				if((fl.LineAngle>0)&&(dci<1612.0)){
					fl.LineAngle=fl.LineAngle-180.0;
				}else if(dci>1612.0){
					fl.LineAngle=180.0+fl.LineAngle;
				}
				
				if(Math.abs(fl.LineAngle-auxAngle)<0.8){
					bw.write(dci+","+fl.LineAngle+"\n");
					System.out.println(dci+","+fl.LineAngle);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			auxAngle=fl.LineAngle;
		}
		
		try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}