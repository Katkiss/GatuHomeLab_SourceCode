package com.sensorlab.devices;

import org.bulldog.core.io.bus.i2c.I2cBus;
import org.bulldog.core.platform.Board;
import org.bulldog.core.platform.Platform;

public class ADCi2c_Accelerometer{
	
	I2cBus bus;
	int address;
	String I2Cbus;
	ADCi2c adc;
	String I2C;

	public ADCi2c_Accelerometer(int address, String I2C) {
		this.address=address;
		this.I2C=I2C;
	}
	
	/*
	 * Starts the communication with the ADC 
	 * getting ready for reading from the Accelerometer
	 * */
	public void init() {
		final Board board = Platform.createBoard();
        I2cBus bus = board.getI2cBus(I2C);
        adc=new ADCi2c(bus,address);
        adc.initMode();
	}
	
	/*
	 * Close the communication with the ADC
	 * */
	public void close(){
		adc.closeMode();
	}
	
	
	/*
	 * Get Accelerometer value from ADC
	 * */
	public int getValue(){
		return adc.readIntValue();
	}

}
