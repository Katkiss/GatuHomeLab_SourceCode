#include <SoftwareSerial.h> //Software Serial Port
#include <string.h>
#define RxD 2
#define TxD 3
SoftwareSerial blueToothSerial(RxD,TxD);
int AnalogIn=5;  //SCL pin is Analog Input 5

void setup(){    
    Serial.begin(9600);      
    //BlueTooth    
    pinMode(RxD, INPUT);    
    pinMode(TxD, OUTPUT);
    //Serial.println("Starting BT...");
    setupBlueToothConnection();   
}

void setupBlueToothConnection()
{
    //Serial.println(F(".."));
    blueToothSerial.begin(38400); //Set BluetoothBee BaudRate to default baud rate 38400
    delay(1000);
    sendBlueToothCommand("\r\n+STWMOD=0\r\n");  //device working as slave (0) master (1)
    sendBlueToothCommand("\r\n+STNA=BTB01-st\r\n"); //device name
    sendBlueToothCommand("\r\n+STAUTO=0\r\n"); // 0 // permit auto connect (1)
    sendBlueToothCommand("\r\n+STOAUT=1\r\n"); // not permit paired device to connect me (0)
    sendBlueToothCommand("\r\n+STPIN=0000\r\n"); // pin code
    //sendBlueToothCommand("\r\n+LOSSRECONN=0\r\n"); // permit auto-reconnecting
    delay(2000); // This delay is required.
    sendBlueToothCommand("\r\n+INQ=1\r\n"); // enable been inquired (1) when slave by master
    delay(2000); // This delay is required.
    Serial.println(F("setupBTconn: listening..."));
    while(!conn){
      if(blueToothSerial.available()>0){
        CheckOK();
        conn=true;
        Serial.println(F("SetupBTconn: ok"));
      }
    }
}

void loop() // run over and over
{
     //read analog signal
      int value=analogRead(AnalogIn);
      Serial.print("Analog Value: ");
      Serial.println(value);
     delay(300); //300
}