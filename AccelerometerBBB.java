package linux.java.process;

import com.sensorlab.devices.ADCi2c_Accelerometer;

public class AccelerometerBBB {

    public static void main(String[] args) {
        ADCi2c_Accelerometer acc=new ADCi2c_Accelerometer(0x50,"I2C_1");
        acc.init();
        for(int i=0;i<100;i++){
            System.out.println("Accelerometer Value: "+acc.getValue());
        }
    }

}