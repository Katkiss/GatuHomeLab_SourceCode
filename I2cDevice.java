package com.sensorlab.devices;

import java.io.IOException;

import org.bulldog.core.io.bus.i2c.I2cBus;
import org.bulldog.core.io.bus.i2c.I2cConnection;
import org.bulldog.core.io.bus.i2c.I2cDevice;
import org.bulldog.core.util.BulldogUtil;

public class ADCi2c extends I2cDevice{
	
	byte REG_ADDR_RESULT=0x00;
	byte REG_ADDR_CONFIG=0x02;
	
	//constructor
	public ADCi2c(I2cBus bus, int address) {
		super(bus, address);
	}

	//constructor
	public ADCi2c(I2cConnection connection) {
		super(connection);
	}
	
	//methods 
	public void initMode() {
		try {
			this.open();
			System.out.println("ADCi2c::initMode:: open ok");
			this.writeByte(REG_ADDR_CONFIG); //select the CONFIGURATION REGISTER OF THE ADC
			System.out.println("ADCi2c::initMode:: write byte 0x02 ok");
			this.writeByte(0x20);//set sampling rate to 27ksps
			System.out.println("ADCi2c::initMode:: write byte 0x20 ok");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("ADCi2c::initMode::error "+e.toString());
		}	
	}
	
	public void closeMode() {
		try {
			this.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("ADCi2c::closeMode::error "+e.toString());
		}
		System.out.println("ADCi2c::closeMode:: close ok");
	}
	
	public double readValue() {
		byte[] buffer = new byte[2];
		try {
			this.writeByte(REG_ADDR_RESULT);//select the result register
			this.readBytes(buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("ADCi2c:readValue::error "+e.toString());
		}
		int intValue = (((BulldogUtil.getUnsignedByte(buffer[0]) << 8)) | BulldogUtil.getUnsignedByte(buffer[1]));
		double value = intValue / 1.0;
		System.out.println("ADCi2c::readValue:: value "+value);
		return value;
	}
	
	public int readIntValue() {
		byte[] buffer = new byte[2];
		try {
			this.writeByte(REG_ADDR_RESULT);
			this.readBytes(buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int intValue = (((BulldogUtil.getUnsignedByte(buffer[0]) << 8)) | BulldogUtil.getUnsignedByte(buffer[1]));
		return intValue;
	}
}
