package com.sensorlab.servers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.bluetooth.LocalDevice;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

import com.intel.bluetooth.BlueCoveImpl;
import com.sensorlab.comm.LinuxBTCommunicationTrama;



public class BTBeeGatuHomeLab {
	
	byte[] start={'S','T','R'};
	byte[] end={'E','T','R'};
	
	BTLogRegisterGatu log=null;
	
	
	//connect to sensors
	private void startConnection(String btAddress) {
			
		System.out.println("Opening Connection...");
		StreamConnection conn = null;
		try {
			conn = (StreamConnection) 
			          //Connector.open("btspp://"+btAddress+":1");
					  Connector.open("btspp://"+btAddress+":1",Connector.READ_WRITE,true);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return;
		}
		
		System.out.println("Opening Input/Output Streams...");

		OutputStream out = null;
		InputStream in=null;
		try {
			out = conn.openOutputStream();
			in = conn.openInputStream();	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Error opening streams "+e.toString());
			return;
		}
		
		System.out.println("Connection with BTBee OK!");
		
		System.out.println("Sending TramaID...");

		LinuxBTCommunicationTrama lbt=new LinuxBTCommunicationTrama(in,out,start,end);
		String minValue=new String("350");
		minValue='#'+minValue+'#';
		//lbt.sendTrama(tramaID);
		lbt.sendTrama(minValue.getBytes());
		lbt.getTrama();
		String stat=lbt.getTramaInfoSt();
		System.out.println("Received from BTBee -- sensor Status: "+stat);
		
		if(stat.equals("0")){
			System.out.println("No interaction");
		}else if(stat.equals("1")){
			System.out.println("Cat Detected!!!!");
		}else{
			System.out.println("No interaction");
		}
		

		try {
			in.close();
			out.close();
			conn.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		
		System.out.println("Connection End");	
	}
	

	
	//static method
	public static void main(String[] args)  {
		//display local device address and name
		try{
			LocalDevice localDevice = LocalDevice.getLocalDevice();
			System.out.println("Address: "+localDevice.getBluetoothAddress());
			System.out.println("Name: "+localDevice.getFriendlyName());
		}catch(IOException ex){
			System.out.print(ex.toString());
		}	
		
		BTBeeGatuHomeLab beeasker=new BTBeeGatuHomeLab();
		beeasker.startConnection("00:13:EF:00:09:6D"); //BT address of BTBee
		//shut down the BlueCove stack
		BlueCoveImpl.shutdown();
	}
}