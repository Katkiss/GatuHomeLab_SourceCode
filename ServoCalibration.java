public class ServoCalibration {    
    
    //variables
    private final Board board;
	private URL url;
	private ServoHS422 servo;
	
    //constructor
	public ServoCalibration(){
	    //get access to GPIO in BBB
		this.board = Platform.createBoard();
		//initialize camera
		url=null;
		try {  //change here the ip that appears in the camera
			url = new URL("http://192.168.1.116:8888/camera&timestamp"); 
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}    
		servo=new ServoHS422(board, BBBNames.EHRPWM0B_P9_22,50.0);
		servo.setCalibration(1500.0, 10.0);
		servo.startPosition(0.0);
	}
    
    
    //method to calibrate the servo using the camera
	public void calibrateServoUsingCamera(){
		//we work directly with the duty cicle... and we will keep increasing with a certain step
		//take image
		//detect the line and angle on the servo using BoofCV
		double dc=500.0; //starting value
		double step=1.0; //increasing step for duty cycle
		double maxdc=2300.0;
		BufferedImage input=null;
		System.out.println("Starting movement...");
		for(double dci=dc;dci<maxdc;dci+=step){
		    //move the servo
			servo.setDutyCycle(dci);
			//getting and saving image
			try {
				input = ImageIO.read(url);
				ImageIO.write(input, "jpg",new File("./images/"+dci+".jpg"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Image ok...");
		}
	}
}