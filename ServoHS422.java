import org.bulldog.core.gpio.Pwm;
import org.bulldog.core.platform.Board;

public class ServoHS422 {
	
	public String address;
	public double freq;
	final Board board;
	public Pwm pwm;
	
	//calibration
	public double a;
	public double b;
	
	//moving range
	public double maxAngle;
	public double minAngle;
	
	
	//constructor
	public ServoHS422(Board board, String address,double freq){
		this.board=board;
		this.address=address;
		this.freq=freq;
		this.pwm = board.getPin(address).as(Pwm.class);
		pwm.disable();
	}
	
	//angle in degrees
	public boolean startPosition(double angle){
		boolean task=true;
		pwm.setFrequency(50.0f);      	        // in Hz
		pwm.setDuty(getDutyCycle(angle));       // % duty cycle
        pwm.enable();
		return task;
	}
	
	//we send the angle in degrees
	public boolean moveToAngle(double angle){
		boolean task=true;
		if((angle>maxAngle)||(angle<minAngle)){
			return false;
		}
		double duty=getDutyCycle(angle);
		if((duty>1.0)||(duty<0.0)){
			return false;
		}
		pwm.setDuty(duty);
		return task;
	}
	
	//method to set the relation between the duty cycle and the angle
	//t=a+bx - t - time in us (microseconds) signal up
	//         x - angle in degrees
	public void setCalibration(double a, double b){
		this.a=a;
		this.b=b;
	}
	
	//method to get duty cycle from angle (in degrees)
	public double getDutyCycle(double angle){
		double t=a+b*angle;
		double dc=(t/1000.0)*(freq/1000.0);
		//System.out.println("ServoHS422 duty Cycle: "+dc+ " angle: "+angle);
		return dc;
	}
	
	//method to close down the use of the servo
	public void shutdownServo(){
		pwm.disable();
	}
	
	//method to set moving range
	public void setRange(double maxAngle, double minAngle){
		this.maxAngle=maxAngle;
		this.minAngle=minAngle;
	}
	
	//method to set position directly with duty cycle
	public void setDutyCycle(double t){
		double dc=(t/1000.0)*(freq/1000.0);
		pwm.setDuty(dc);
	}

}